# Technology and Policy for Electrical Energy Storage
## Lectures
Prof. Tobias Schmidt, Prof. Vanessa Wood

# Course
D-GESS at ETHZ

# Analysis direct links
[S01](Analysis/S01.md)
[S02](Analysis/S02.md)
[S03](Analysis/S03.md)
[S04](Analysis/S04.md)
[S05](Analysis/S05.md)