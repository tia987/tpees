# S09

## Slides
### Rationales for public policy intervention
Public policies can help accelerate/ re-direct technological changes, when policy makers induce innovation. 
They should offer long-term targets and minimize first-mover disadvantages, shape new systems and markets, and raise demand in sectors desired. 
Policy intervention is usually justified when **market failures** exist:
- Public goods such as infrastructure and education are undersupplied by private sectors
    - non-rival: one person receiving the service doesn't prevent others from doing so
    - non-excludable: no garantee that only people who paid have access
- Externalities affect other parties who are not involved $\Rightarrow$ Costs need to be internalized e.g. through taxes and benefits need to be protected e.g. through patents. 
    - positive: higher education lowers crime rates, knowledge spillovers benefits other students
    - negative: pollutions cause environmental damages
- Market powers (monopolies or cartels) offer too high prices
- Information asymmetry

### Technological change and experience
Investments in researches and knowledges are not garanteed for return (sunk costs/ upfront costs). First-mover disadvantages (ultimate results may let other companies benefit) further makes innovations difficult. Policy makers must step in:
- Technology push policy: support positive externalities by investing in inventions
- Demand pull policy: set emission goals or inject subsidies to help the technology diffuse into markets
When new products are introduced to the market, they may not be immediately successful due to lack of experience with the technology. In such cases, subsidies may be necessary to encourage adoption.

### Experience curves
The more a technology is used, the cheaper it gets with "experience". The learning rate is expressed as $LR=1-PR=a1-2^b$. According to some studies in 2015 the costs for EV was suppposed to decrease, in the study presented in the lecture, the learning rate has been underestimated, but this happened because there was not enough data points to predict accurately the demand and the use of such Electric Vehicles, as now as 2023. But for other product such as different kind of batteries is possible to see that usually the price falls with time and with the experience. ![Li-ion cost redution](Pictures\LI_ION_Cost_reduction.png)

### Implications for public policy
There are several implications for public policy when it comes to technological change and experience:
- **Some technogies become dominant**: The more user experiences they gain, the more advanced they are inside the sector $\Rightarrow$ outcompeting newly introduced technologies, leading lock in/out of technologies
- **Need for demand** $\Rightarrow$ need for pull policies
- **Cheaper with time** $\Rightarrow$ need for flexible policy to adjust subsidy levels 
- **Multi-purpose technology** 

![Learning curve](Pictures\Learning_curve.png)

The bigger the market share is, the more a company can sell its product. While market share is an important factor in a company's success, external factors like material availability can also greatly impact the final price of a product.
In case of the example of EVs, the COVID pandemic led to turbolences for the chip manifacturing, which disrupted the production and increasing the demand. External factors such as material aviability can affect extremely the final price also for batteries, one example is how USA is actually investing in builiding more lithium "farms",  which may lead to cheaper components and lower battery prices.

#### Stacking applications for MPT
Multi-purpose technolgies (MPT) have multiple distinct applications. When such new technologies emerge, public policies can insert demand pull policies in different markets to minimize the economic cost of subsidy. For lithium-ion batteries, whose applications range wide in different sectors, the self-reinforcing mechanism is already sufficient to increase the demand. 
> In the early 2000s, batteries are mainly used in *consumer electronics* such as phones or other small devices. When the market of EV batteries is developed, the use of batteries in cars skyrocketed. By assigning different performace and characteristics with the progress of techonolgy and market, batteries have the possibilities to engage in even more markets. ![Battery applications](Pictures\Battery_applications.png)

However, public policies usually prevents application stacking by requiring pre-qualifications, unbundling etc.


## Paper
### Introduction
[Kvist Nilsson](https://moodle-app2.let.ethz.ch/pluginfile.php/1491288/mod_resource/content/3/Nykvist_Nilsson_2015_NCC_Battery%20costs.pdf) Is mandatory for this week. The rest of the readings are related to [Ferioli](https://moodle-app2.let.ethz.ch/pluginfile.php/1491284/mod_resource/content/0/Ferioli_2009_learning%20curves.pdf) and [Stephan et al](https://moodle-app2.let.ethz.ch/pluginfile.php/1547200/mod_resource/content/0/Stephan%20et%20al_2017_The%20sectoral%20configuration%20of%20TIS.pdf).

### Kvist Nilsson
The paper analyse the costs oof Lithium-Ion batteries in electriv vehicles, and shows the declining in price of such technology and the consequent increase of such kind of cars on the global market. The researcher estimate a decline in production costs of the 14% between 2007-2014. The most important factor in the achievement of such reduction related to the market of *BEV* is its relative costs, and the most important point for the production of such cars and its popularity is determined by the battery. The papers denotes also that improvements in terms battery material still must be made.

> There are improvements to be made in anode and cathode materials, separator stability and thickness, and electrolyte composition...material cost is among the most important

The researcher estimates an 8% annual cost decline related to the market actors, and a generic technological advancement for this specific industry. Also a parity in costs with the more common combustion cars is expected, which would set an attractivity forwards the *BEV*.

> However, if costs reach as low as US$150 per kWh this means that electric vehicles will probably...begin to penetrate the market widely, leading to a potential paradigm shift in vehicle technology

Concluding, the paper denote that life-cycle and enviromental impact of such technology will affect the market, even though big manifacturers such as Tesla and LG invested heavily in this market, and also a public support, with economic incentives espect to boost this sector.