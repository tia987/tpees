# S05

## Slides
### Safety Mechanisms
Longer cycle life, lower cost and faster charge are desired, if batteries are expected to operate over a longer time period.
#### Inside cell: 
- materials (higher temperature endurance), shutdown separators
- thermistor: increases resistance with temperature
- current interrupt device: cuts off the circuit in case of excessive cell pressure, high temperature or voltage
- safety vents
#### Outside cell:
- Battery management system: estimates state-of-function (SOF) based on state-of-charge (SOC) and state-of-health (SOH, *percentage of unusable capacity to total capacity*) 
- Thermal fuses
### Degradation
During charge and discharge, electrochemical, mechanical, and thermal effects can cause damages to the cell.
#### Cycle Life
- Influences of charge and discharge on the capacity before it drops below 80% 
- **Coulombic Efficiency**: Extracted charge from the total inserted charge $\rightarrow$ predict lifetime of a cell

    $\eta_{coulombic} = \dfrac{Q_{discharge}}{Q_{charge}} = \dfrac{\int_{discharge}{I dt}}{\int_{charge}{I dt}}$

- For batches of battery, the exact differences between long and short cycle lives can hardly be determined within the first 100 cycles. 
- Depending on the application, manufacturers would prefer wider cycle life distribution or higher homogenity within the batch.
#### Limitation and Benefits: Energy vs. Power Cell
Different design choices for the same materials
- **Energy cell**: higher gravimetric and volumetric energy density, thicker electrodes, longer charge/discharge $\Rightarrow$ Charge and discharge are **chemestry limited**.
    If charge/discharge at higher C-Rates, the usuable capacity will quickly drop below 80%.
- **Power cell**: lower gravimetric and volumetric energy density, thinner electrodes (*shorter pathlengths for Li-ions, current limitations*), fast charge & discharge.
    Charge is chemistry limited (less than energy cell), discharge is **temperature limited**.

### Batteries for Electric Vehicles and Transports
All-electric driving will limit battery operation range to perserve performance (charge sustaining mode: battery never drops too much).
- Micro hybrids (low voltage hybrids): powers auxilary systems (e.g. air conditioning, radio)
- Mild hybrids (high voltage hybrids): no all electric drive possible, discharged for acceleration
- Plug-in Hybrid electric vehicles (*PHEV*): All electric drive possible (but not necessary), external charge of battery
- Full electric vehicles (*BEV*, Battery Electric Vehicle): fulfill all driving requirements, driving range directly related to battery capacity

## Exercises
### Define the term Coulombic Efficiency.
The coulombic efficiency is the ratio between extracted charge and inserted charge. 

### Will the Coulombic Efficiency of a cell change for different C-Rates? Why?
No, because the inserted and extracted charge don't depend on the C-Rates.

### If a power and energy cell are made using identical materials, how will their design vary? Explain.
Energy cells will have thicker electrodes and higher energy density. It will be used to support longer charge and discharge cycles. Power cells have thin electrodes, which minimizes the pathlengths for lithium ions and enables fast charging processes.

### What limits a power cell during discharge? What limits an energy cell during discharge? 
A power cell is limited by temperature during discharge, an energy cell by chemistry.

### Describe 3 degradation effects in lithium-ion cells and their impact on cell performance.
Lithium-ion cells can experience electrochemical, mechanical, and thermal effects during charge and discharge. Electrochemical degradations can cause e.g. electrolyte leakage and corrosions. Mechanical degradations can cause structural changes such as fracture, delamination, or stress. Thermal effects are mostly resistive heating of the materials.

### Briefly define the different types of electrified vehicles (hybrid, plug-in hybrid, and battery electric vehicle).
For mild hybrid types, no all-electric drive is possible. Batteries only serve as support for auxiary systems or accelerations. For the other two types, all-electric drive are possible. PHEV supports battery as a secondary energy source, whilst BEV rely completely on batteries.

### What is a typical voltage and current needed for a BEV battery? What about energy and Power?
The voltage is usually in the range of 300-400V. A current of several hundred amperes are needed. A typical BEV may consume around 15-20kWh of energy per 100km of driving, and depending on the size of the vehicle, the motor may have a power output between 30-200kW.

### If cells with specifications Pouch, 25Ah 3.7V are used, how many are needed and how can they be connected to achieve a BEV traction battery with capacity and nominal voltage calculated in #7?
We must first connect the batteries in series to add up the total voltage. Approximately 100 cells are required to reach around 400V. The series circuits then must be connected in parallel, such that the currents are summed. Four to five of the series circuits are needed. 

[Solutions](https://moodle-app2.let.ethz.ch/mod/page/view.php?id=889786)